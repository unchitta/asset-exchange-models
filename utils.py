import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

def histogram(data, bins):
    from collections import defaultdict
    xmin = min(data)
    xmax = max(data)
    dx = float(xmax-xmin)/bins
    H = defaultdict(int)
    for x in data:
        if x == xmax:
            q = bins
        else:
            q = int((x-xmin)/dx)+1 
        H[q]+=1
    xcoord ={}
    for q in H.keys():
        xcoord[q]=xmin+(q-1)*dx # Using left end of bin.
        #xcoord[q]=xmin+(q-0.5)*dx # This would be mid-bin.
    return xcoord, H

def discrete_histogram(data):
    from collections import defaultdict
    kmin = min(data)
    kmax = max(data)
    hist = defaultdict(int)
    for j in data:
        hist[j] += 1
    return hist

def compute_cdf(data, bins):
    counts = len(data)
    data = np.array(data)
    cdf_hist_xcoord = np.linspace(min(data),max(data),bins)
    cdf_hist = []
    for x in cdf_hist_xcoord:
        cdf_hist.append(np.sum(data > x))
    cdf_sum = sum(cdf_hist)
    cdf_hist = [x/counts for x in cdf_hist]
    cdf = dict(zip(cdf_hist_xcoord, cdf_hist))
    return cdf

def compute_pdf(data, bins):
    counts = len(data)
    hist_xcoord, hist = histogram(data, bins)
    pdf = dict(zip(hist_xcoord.values(), [x/counts for x in hist.values()]))
    return pdf

def plot_dis(data, kind='pdf', bins=1000):
    if kind=='pdf':
        dis = compute_pdf(data, bins)
    elif kind=='cdf':
        dis = compute_cdf(data, bins)
    else:
        return None
    fig, ax = plt.subplots(figsize=(5,3), dpi=300)
    ax.scatter(list(dis.keys()), list(dis.values()), color='white', edgecolors='black', linewidths=0.4, s=10)
    plt.loglog()
    sns.despine(top=True, right=True)
    return fig, ax

def lorenz_curve(data):
    counts = len(data)
    n = int(0.01*counts)  # this is the percent
    data_sorted = sorted(list(data))
    sum_data = sum(data_sorted)
    lc_xcoord = np.arange(0,101,1)
    lc = []
    for x in lc_xcoord:
        i = n*x
        lc.append(sum(data_sorted[:i])/sum_data)
    lc[-1] = 1.0

    linex = np.arange(0,101,1)
    liney = np.arange(0,1.01,0.01)
    gini = np.trapz(lc, x=lc_xcoord) / np.trapz(liney, x=linex)
    return lc_xcoord, lc, gini

def plot_lorenz_curve(data):
    lc_xcoord, lc, gini = lorenz_curve(data)
    fig = plt.figure(figsize=(3,3), dpi=300)
    linex = np.arange(0,101,1)
    liney = np.arange(0,1.01,0.01)
    plt.plot(lc_xcoord, lc, c='black')
    plt.fill_between(lc_xcoord, liney, color='lightblue',alpha=0.5)
    plt.fill_between(lc_xcoord, lc, color='lightseagreen', alpha=0.8)
    plt.plot(linex, liney, c='black')
    plt.axvline(100, 0.05,0.95, c='black')
    plt.axhline(0, 0.05, 0.95, c='black')
    plt.xlabel('Cum. % of People from Lowest to Highest Income', fontsize='small')
    plt.ylabel('Cum. % of Income Earned', fontsize='small')
    plt.text(5,0.80,f'Gini Coefficient: {np.round(gini,3)}', fontsize='small')
    sns.despine(top=True, right=True)
    plt.show()