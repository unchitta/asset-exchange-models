"""
Asymmetric asset exchange model with/without networks.

References
Sinha, S., 2005. The rich are different! Pareto law from asymmetric interactions in asset exchange models. In Econophysics of Wealth Distributions (pp. 177-183). Springer, Milano.

Gusman, S.R., Laguna, M.F. and Iglesias, J.R., 2005. Wealth distribution in a network with correlations between links and success. In Econophysics of Wealth Distributions (pp. 149-158). Springer, Milano.

"""


import random
import networkx as nx

class Agent:
    def __init__(self, w0, tau):
        self.init_wealth = w0
        self.wealth = w0
        self.tau = tau


class AsymmetricAssetExchangeModel:
    def __init__(self, N, w0, tau, network=False, k=None, seed=None):
        """Instantiates an instance of this model.

        Args:
            N (int):  number of agents
            w0 (sequence): list of initial wealth endowments of agents
            tau (sequence): list containing the thrift parameter for each of the N agents
            network (bool, optional): Whether or not to implement trade networks. Defaults to False.
            k (int, optional): If network is True, provide an average degree for an initial G(N,p) random network here. Defaults to None.
            seed (int, optional): seed of the random state. Defaults to None.
        """
        self.agent_ids = list(range(N))
        self.agents = []
        for i in range(N):
            self.agents.append(Agent(w0[i], tau[i]))
        random.seed(seed)
        random.shuffle(self.agents)
        if network:
            self.G0 = nx.fast_gnp_random_graph(N, k/N)
            self.links = list(self.G0.edges())
        self.network = network
    

    def run(self, t, T):
        """Runs the model for t periods. 
        
        The rewiring of links (if network is enabled) happens every T periods.

        Args:
            t (int): model ticks
            T (int): period of link rewiring
        """

        def _exchange(agent1,agent2):
            w1 = agent1.wealth
            w2 = agent2.wealth
            t2 = agent2.tau
            # compute asset exchange amount using the asymmetric model
            # assumes agent 1 wins the net exchange
            epsilon = random.random()
            delta_w = epsilon*(1 - t2*(1 - w1/w2))*w2 if w1 <= w2 else epsilon*w2
            # exchange and update wealth
            agent1.wealth = agent1.wealth + delta_w
            agent2.wealth = agent2.wealth - delta_w if agent2.wealth - delta_w > 0 else 0

        if not self.network:
            for _ in range(t):
                # select a pair of agents at random
                pair = random.choice(self.agents, 2)
                agent1 = pair[0]
                agent2 = pair[1]
                _exchange(agent1, agent2)
        else:
            for T in range(int(t/T)):
                for _ in range(T):
                    # select a pair of agents from the network
                    i,j = random.choice(self.links)
                    if random.random() <= 0.5:
                        agent1 = self.agents[i]
                        agent2 = self.agents[j]
                    else:
                        agent1 = self.agents[j]
                        agent2 = self.agents[i]
                    # agents perform transaction
                    _exchange(agent1,agent2)

                # a random "succesfull" agent takes a link to itself
                p = [agent.wealth for agent in self.agents]
                successful_agent = random.choices(self.agent_ids, weights=p)[0]
                (u,v) = random.choice(self.links)
                new_link = (successful_agent,u) if random.random() <= 0.5 else (successful_agent,v)
                while new_link in self.links:
                    (u,v) = random.choice(self.links)
                    new_link = (successful_agent,u) if random.random() <= 0.5 else (successful_agent,v)
                self.links.remove((u,v))
                self.links.append(new_link)


    def get_all_wealth(self):
        """Returns a list of the wealth of all agents in the system

        Returns:
            list: list of all agents' wealth
        """
        return [agent.wealth for agent in self.agents]


    def get_network(self, t0=False):
        """Returns the trade network of this instantiation.

        Args:
            t0 (bool, optional): Whether or not to return the initial network. Defaults to False.

        Returns:
            nx.Graph: a NetworkX Graph object representing the trade network.
        """
        if not self.network:
            return None
        elif t0:
            return self.G0
        else:
            G = nx.Graph()
            G.add_nodes_from(self.agent_ids)
            G.add_edges_from(self.links)
            return G


    def get_degree_histogram(self, t0=False):
        """Returns the degree histogram of the transaction network.
        If network is not enabled in this instance, this will return None.

        Args:
            t0 (bool, optional): Whether or not to return the degree histogram of the network at t=0. Defaults to False.

        Returns:
            dict: the keys of this dict is the degree, and the values are the counts
        """

        if not self.network:
            return None

        from collections import defaultdict
        G = self.get_network(t0)
        degrees = [G.degree(i) for i in self.agent_ids]
        hist = defaultdict(int)
        for j in degrees:
            hist[j] += 1
        return dict(hist)
